# This script performs a migration of the virtualenv (AAP 1) to the corresponding EE images (AAP 2)
import json
import logging

import click
import requests

@click.command()
@click.option("--mapping-list-ee", help="List of virtual env and their corresponding ee image. A json string is "
                                        "expected.")
@click.option("--aap-url", help="Base url of the aap controller to migrate. e.g: https://aap-local.dv")
@click.option("--aap-token", help="Token of the aap controller to migrate")
@click.option("--entities-to-patch", help="List of entities to migrate. Coma (,) separated.",
              default="projects,job_templates,organizations")
@click.option("--ignore-cert-validation", is_flag=True, default=False)
def migrate_venv_to_ee(mapping_list_ee: str, aap_url: str, aap_token: str, entities_to_patch: str,
                       ignore_cert_validation: bool = False):
    """

    :param mapping_list_ee: a list of virtual env and their corresponding ee image.
                            A json string is expected:
                            ```json
                            [
                                {"aap_1": "/var/lib/awx/venv/ansible", "aap_2": "my_ansible_ee"},
                                {"aap_1": "/var/lib/awx/venv/ansible_other", "aap_2": "my_ansible_ee2"}
                            ]
                            ```
    :param aap_url: base url of the aap controller to migrate. e.g: https://aap-local.dv
    :param aap_token: token of the aap controller to migrate
    :param entities_to_patch: list of entities to migrate. Coma (,) separated.
    :param ignore_cert_validation: Ignore TLS verification
    """

    certs_validation = not ignore_cert_validation
    mapping_list_ee = json.loads(mapping_list_ee)
    aap_url = aap_url.rstrip("/")
    entities_to_patch = entities_to_patch.split(",")

    headers_aap = {
        "Authorization": f"Bearer {aap_token}",
        "Content-Type": "application/json"
    }

    # 1. Construct the mapping list dict custom_venv => image id
    list_ee = _list_all_ee_images(aap_url, headers_aap, certs_validation)
    translate_map_ee = {}

    for mapping_ee_item in mapping_list_ee:
        if mapping_ee_item["aap_2"] not in list_ee.keys():
            logging.error(f"The EE image '{mapping_ee_item['aap_2']}' cannot be found on the AAP controller, this is "
                          f"likely due to the absence of the EE image, please check.")
            exit(1)

        translate_map_ee[mapping_ee_item["aap_1"]] = list_ee[mapping_ee_item["aap_2"]]
        logging.info(f"Added EE mapping: {mapping_ee_item['aap_1']} => {mapping_ee_item['aap_2']} "
                     f"(#{list_ee[mapping_ee_item['aap_2']]})")

    # 2. Patching each entity
    for entity_name in entities_to_patch:
        logging.info(f"Patching venv to EE for entity '{entity_name}'")

        url_entity_page = f"{aap_url}/api/v2/{entity_name}/?page_size=200&order_by=id"

        while True:
            logging.info(f"Listing {entity_name}: {url_entity_page}")
            entity_list_req = requests.get(url_entity_page, headers=headers_aap, verify=certs_validation)

            entity_list_req.raise_for_status()
            entity_list_req = entity_list_req.json()

            for entity in entity_list_req["results"]:
                try:
                    _migrate_entity_ee(
                        aap_url=aap_url, aap_headers=headers_aap, certs_validation=certs_validation,
                        entity=entity, translate_map_ee=translate_map_ee
                    )
                except requests.exceptions.RequestException as e:
                    if e.response is not None:
                        logging.error(f"An error occurred on url={e.response.request.url},"
                                      f"code={e.response.status_code}, request_method={e.response.request.method},"
                                      f"request_body={e.response.request.body}"
                                      )
                    else:
                        logging.error(f"An error occurred on url={e.request.url},"
                                      f"request_method={e.request.method}, request_body={e.request.body}"
                                      )

            if entity_list_req["next"]:
                url_entity_page = f"{aap_url}{entity_list_req['next']}"
            else:
                break

        logging.info(f"Migration of {entity_name} complete.")
    logging.info("Migration complete.")


def _list_all_ee_images(aap_url: str, aap_headers: dict, certs_validation: bool = True):
    """
    List all the EE images on the AAP controller.

    :param aap_url: base url of the aap controller to migrate
    :param aap_headers: headers of the aap controller
    :param certs_validation: TLS verification

    :return a dict of ee images. `{image_name_1: image_id_1, image_name_2: image_id_2}`
    """

    ee_list = {}
    ee_list_url = f"{aap_url}/api/v2/execution_environments/?page_size=200&order_by=id"
    while True:
        ee_list_req = requests.get(
            ee_list_url,
            headers=aap_headers, verify=certs_validation
        )

        ee_list_req.raise_for_status()
        ee_list_req = ee_list_req.json()

        for ee_image in ee_list_req["results"]:
            ee_list[ee_image["name"]] = ee_image["id"]

        if ee_list_req["next"]:
            ee_list_url = f"{aap_url}{ee_list_req['next']}"
        else:
            break

    return ee_list

def _migrate_entity_ee(aap_url, aap_headers, certs_validation, entity, translate_map_ee):
    pass

if __name__ == '__main__':
    migrate_venv_to_ee(auto_envvar_prefix="AAP_MIGRATE_EE")
